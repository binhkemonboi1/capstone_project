var header = document.getElementById("Site-Header");
var topHeader = document.getElementById("topHeader");
var navbar = document.getElementById("navbar");

window.addEventListener("scroll", function () {
  var sclY = this.scrollY;
  if (sclY > 54) {
    navbar.style.transition = `all .4s`;
    navbar.style.height = "40px";
    header.style.paddingTop = `0px`;
    header.style.boxShadow = `0 3px 6px 0 rgb(0 0 0 / 7%)`;
    header.style.zIndex = `5`;
    header.style.position = `fixed`;
    header.style.width = "100%";
    topHeader.style.position = "absolute";
  } else {
    navbar.style.height = "60px";
    navbar.style.transition = `all .4s`;
    header.style.zIndex = `4`;
    header.style.position = `absolute`;
    header.style.boxShadow = `none`;
    header.style.width = "100%";
    topHeader.style.position = "relative";
  }
});